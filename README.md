# Commerce add to cart confirmation

This module adds an "add to cart confirmation" view mode to commerce_product
entities, and uses it to show the newly added product in the confirmation
dialog.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/commerce_add_to_cart_confirmation).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/commerce_add_to_cart_confirmation).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:

- [Commerce Core](https://www.drupal.org/project/commerce)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

This module has no menu nor modifiable settings.
