<?php

namespace Drupal\commerce_add_to_cart_confirmation\EventSubscriber;

use Drupal\commerce_add_to_cart_confirmation\CartConfirmationManager;
use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber ConfirmationMessageSubscriber.
 */
class ConfirmationMessageSubscriber implements EventSubscriberInterface {

  /**
   * The cart confirmation manager.
   *
   * @var \Drupal\commerce_add_to_cart_confirmation\CartConfirmationManager
   */
  protected $cartConfirmationManager;

  /**
   * Constructs a new ConfirmationMessageSubscriber instance.
   *
   * @param \Drupal\commerce_add_to_cart_confirmation\CartConfirmationManager $cart_confirmation_manager
   *   The cart confirmation manager.
   */
  public function __construct(CartConfirmationManager $cart_confirmation_manager) {
    $this->cartConfirmationManager = $cart_confirmation_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[CartEvents::CART_ENTITY_ADD][] = ['onAddToCart'];
    return $events;
  }

  /**
   * Handles the add to cart event.
   *
   * @param \Drupal\commerce_cart\Event\CartEntityAddEvent $event
   *   The cart entity add event.
   *
   * @throws \Exception
   */
  public function onAddToCart(CartEntityAddEvent $event): void {
    $this->cartConfirmationManager->recordAddToCart([
      'order_item_id' => $event->getOrderItem()->id(),
      'quantity' => $event->getQuantity(),
    ]);
  }

}
