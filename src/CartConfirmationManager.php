<?php

namespace Drupal\commerce_add_to_cart_confirmation;

use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Provides CartConfirmationManager service.
 */
class CartConfirmationManager implements CartConfirmationManagerInterface {

  /**
   * Constructs a new CartConfirmationManager object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStore
   *   The private store.
   */
  public function __construct(
    protected PrivateTempStoreFactory $privateTempStore,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function recordAddToCart(array $cart_item_info): void {
    $this->getStore()->set('cart_item_info', $cart_item_info);
  }

  /**
   * {@inheritdoc}
   */
  public function getCartItemInfo(): ?array {
    $cart_item_info = $this->getStore()->get('cart_item_info');
    if ($cart_item_info) {
      $this->clear();
    }
    return $cart_item_info;
  }

  /**
   * Gets the private store.
   *
   * @return \Drupal\Core\TempStore\PrivateTempStore
   *   The private store.
   */
  protected function getStore(): PrivateTempStore {
    return $this->privateTempStore->get('commerce_add_to_cart_confirmation');
  }

  /**
   * Deletes the cart item info.
   */
  protected function clear(): void {
    $this->getStore()->delete('cart_item_info');
  }

}
