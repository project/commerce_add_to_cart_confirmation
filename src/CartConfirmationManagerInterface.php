<?php

namespace Drupal\commerce_add_to_cart_confirmation;

/**
 * Interface for the CartConfirmationManager service.
 */
interface CartConfirmationManagerInterface {

  /**
   * Records data about the cart item.
   *
   * @param array $cart_item_info
   *   The array of data e.g order_item ID, quantity.
   */
  public function recordAddToCart(array $cart_item_info): void;

  /**
   * Gets the cart item info.
   *
   * @return array|null
   *   The cart item info.
   */
  public function getCartItemInfo(): ?array;

}
