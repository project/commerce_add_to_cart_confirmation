(function (Drupal, drupalSettings) {
  Drupal.behaviors.commerce_add_to_cart_confirmation = {
    attach(context) {
      once('commerce-add-to-cart-confirmation', 'body').forEach(function () {
        if (
          drupalSettings?.commerce_add_to_cart_confirmation?.content !==
          undefined
        ) {
          const popupContent =
            drupalSettings.commerce_add_to_cart_confirmation.content;
          const popupTitle =
            drupalSettings.commerce_add_to_cart_confirmation.title;

          const confirmationModal = Drupal.dialog(
            `<div>${popupContent}</div>`,
            {
              title: popupTitle,
              dialogClass: 'commerce-confirmation-popup',
              width: 745,
              height: 375,
              maxWidth: '95%',
              autoResize: true,
              resizable: false,
              close(event) {
                event.target.remove();
              },
            },
          );
          confirmationModal.showModal();
          document
            .querySelector('.commerce-add-to-cart-confirmation-close')
            .addEventListener('click', function (event) {
              confirmationModal.close();
            });
        }
      });
    },
  };
})(Drupal, drupalSettings);
